### Step 1: Upload and prepare data for analysis

## 1.1 calculate values for the 'Ethnic' variable

# load packages
library(tidyverse)
library(ggrepel)
library(readxl)
library(export)

# load census data on ethnicity: census_data
census_data <- read_csv(file = "community_ethnic_data.csv")

# find mean ethinicity values for each community: ethnic_val
ethnic_val <- census_data %>% 
  group_by(Community) %>% 
  transmute(mean_fn = round(mean(FN_percent, na.rm = TRUE), 2),
            mean_mt = round(mean(MT_percent, na.rm = TRUE), 2), 
            mean_other = round(mean(Other_percent, na.rm = TRUE),2)) %>%
  distinct() %>% 
  mutate(ethnic = case_when(mean_fn > 75 ~ "FND",
                            mean_mt > 75 ~ "MTD",
                            mean_fn >= 60 & mean_fn <= 75 ~ "FNM",
                            mean_mt >= 60 & mean_mt <= 75 ~ "MTM",
                            mean_mt < 60 & mean_fn < 60 ~ "MX")) %>% 
  arrange(Community) %>% 
  rename(poll = Community)

## 1.2 add ethnicity data to voting data, arrange data for analysis

# read voting data for Athabasca constituency: elections_list
# note that the workbook contains multiple sheets
elections_list <- excel_sheets("athabasca_elections_1991-2016.xlsx") %>% 
  set_names() %>% 
  map(read_excel, path = "athabasca_elections_1991-2016.xlsx")

list2env(elections_list, .GlobalEnv)

# add the "year" column
athabasca_1991$year <- rep(1991, count(athabasca_1991))
athabasca_1995$year <- rep(1995, count(athabasca_1995))
athabasca_1999$year <- rep(1999, count(athabasca_1999))
athabasca_2003$year <- rep(2003, count(athabasca_2003))
athabasca_2007$year <- rep(2007, count(athabasca_2007))
athabasca_2011$year <- rep(2011, count(athabasca_2011))
athabasca_2016$year <- rep(2016, count(athabasca_2016))

# bind into one dataframe: elections_data
elections_data <- bind_rows(athabasca_1991, athabasca_1995,
                            athabasca_1999, athabasca_2003,
                            athabasca_2007, athabasca_2011,
                            athabasca_2016) %>% 
  arrange(year) 

# unclutter workspace
rm(list = ls(pattern = "athabasca"))

# rename variables in line with Tidyverse Style Guide
elections_data <- rename(elections_data, poll = Poll, ndp = NDP, 
                         lib = LIB, pc = PC, sp = SP, gp = GP, 
                         indep = Indep, rejected = Rejected, 
                         eligible = Eligible,counted = Counted)

# rearrange columns in the dataframe for convenience
elections_data <- elections_data[c("year", "poll", "ndp", "sp",
                                   "lib", "pc", "gp", "indep",
                                   "eligible", "counted", "rejected")]

# add data on ethnic composition
elections_data <- left_join(elections_data, ethnic_val, by = "poll")

# ethnic as factor
elections_data <- elections_data %>% 
  mutate_at(vars(ethnic), as.factor) %>%
  # NaN as NA
  na_if("NaN")

# convert to long format for ggplot2: elections_data_long
elections_data_long <- elections_data %>% 
  rename(NDP = ndp, Saskatchewan = sp, Liberal = lib, 
         "Progress. Cons." = pc, Green = gp, Independents = indep) %>% 
  gather(key = party, value = votes, NDP, Saskatchewan, Liberal,
         "Progress. Cons.", Green, Independents) %>% 
  mutate_at(vars(party), as.factor) %>% 
  arrange(year, party)

# re-arrange columns for viewing convenience
elections_data_long <- 
  elections_data_long[c("year", "party", "poll", "votes",
                        "eligible", "counted", "rejected",
                        "mean_fn", "mean_mt", "mean_other", 
                        "ethnic")]

## 1.3. calculate each party's percentage by ethnicity
party_percent_ethnic <- #* see note below
  elections_data_long %>% 
  drop_na(ethnic, votes) %>% 
  group_by(year, party, ethnic) %>% 
  summarize(percent = round(sum(votes)/sum(counted)*100, 2))
#* note: proportions stay the same as if elections_data was used instead 
# due to data being duplicated exact same number of times 
# across rows when converted to a long dataset

## 1.4. calculate each party's percent in select communities
party_percent_select <- elections_data_long %>% 
  filter(poll == "Beauval" | poll == "Cole Bay" | poll == "Jans Bay" |
           poll == "Ile a la Crosse" | poll == "La Loche") %>% 
  drop_na(votes) %>% 
  group_by(year, party, poll) %>% 
  summarize(percent = round(sum(votes)/sum(counted)*100, 2))

## 1.5. calculate voter turnout by ethnicity: turnout_ethnic
turnout_ethnic <- elections_data_long %>% 
  drop_na(ethnic) %>% 
  group_by(year, ethnic) %>% 
  summarize(turnout = round(sum(rejected + counted)/sum(eligible)*100, 2))

## 1.6. calculate voter turnout in select communities
turnout_select <- elections_data_long %>% 
  filter(poll == "Beauval" | poll == "Cole Bay" | poll == "Jans Bay" |
           poll == "Ile a la Crosse" | poll == "La Loche") %>% 
  group_by(year, poll) %>% 
  summarize(turnout = round(sum(rejected + counted)/sum(eligible)*100, 2))

## 1.7. calculate overall voter turnout by year
turnout <- elections_data_long %>% 
  group_by(year) %>% 
  summarize(turnout = round(sum(rejected + counted)/sum(eligible)*100, 2))

## 1.8. calculate total number of eligible voters by ethnic composition
eligible_ethnic <- elections_data %>% 
  drop_na(ethnic) %>% 
  group_by(year, ethnic) %>% 
  summarize(voters_ethnic = sum(eligible))

### Step 2: Explore data: visualization

## 2.1. plot 1991-2016 Athabasca election results, total votes
plot_votes <- 
  filter(elections_data_long, poll == "Total") %>% 
  drop_na(votes) %>% 
  ggplot(aes(x = year, y = votes)) + 
  geom_point(aes(color = party), size = 4) + 
  geom_line(aes(color = party), size = 1.2, 
            show.legend = FALSE) + 
  scale_x_continuous(limits = c(1990, 2017), 
                     breaks = c(seq(1991, 2013, by = 4), 2016))+
  scale_y_continuous(breaks = seq(0, 3500, by = 250)) +
  scale_color_manual(name = "Party",
                     values = c(NDP = "darkorange", 
                                Saskatchewan = "darkgreen",
                                Liberal = "red2",
                                "Progress. Cons." = "navy",
                                Green = "yellowgreen",
                                Independents = "grey35")) +
  theme_bw() +
  theme(plot.title = element_text(hjust = .5, size = 13,
                                  margin = margin(b = 8),
                                  face = "bold"),
        panel.grid.major = element_line(colour = "gray"),
        panel.grid.minor = element_blank(),
        axis.title = element_text(size = 12, face = "bold"),
        axis.title.y = element_text(margin = margin(r = 8)),
        axis.text = element_text(size = 10),
        legend.title = element_text(size = 11, face = "bold"),
        legend.text = element_text(size = 11),
        plot.caption = element_text(margin = margin(t = 8),
                                    hjust = 0)) +
  labs(x = NULL, y = "Votes",
       title = "Number of votes by party, Athabasca 1991-2016",
       caption = "Data source: Elections Saskatchewan, Election Results. https://www.elections.sk.ca/election-results/")

## 2.2. plot 1991-2016 Athabasca election results, percentages   
plot_percent <- 
  filter(elections_data_long, poll == "Percent") %>% 
  drop_na(votes) %>% 
  ggplot(aes(x = year, y = votes, color = party)) + 
  geom_point(size = 3) + 
  geom_line(size = 1.2, show.legend = FALSE) + 
  geom_label_repel(aes(label = votes),
                   fontface = "bold",
                   label.size = .4, # label border thickness
                   size = 3.5, # label size
                   force = .02, # repelling force
                   show.legend = FALSE) +
  expand_limits(ymin = 0, ymax = 90) +
  scale_x_continuous(limits = c(1990, 2017), 
                     breaks = c(seq(1991, 2013, by = 4), 2016)) +
  scale_y_continuous(breaks = seq(0, 100, by = 10)) +
  scale_color_manual(name = "Party",
                     values = c(NDP = "darkorange", 
                                Saskatchewan = "darkgreen",
                                Liberal = "red2",
                                "Progress. Cons." = "navy",
                                Green = "yellowgreen",
                                Independents = "grey35")) +
  theme_bw() +
  theme(plot.title = element_text(hjust = .5, size = 13,
                                  margin = margin(b = 8),
                                  face = "bold"),
        panel.grid.major = element_line(colour = "gray"),
        panel.grid.minor = element_blank(),
        axis.title = element_text(size = 12, face = "bold"),
        axis.title.y = element_text(margin = margin(r = 8)),
        axis.text = element_text(size = 10),
        legend.title = element_text(size = 11, face = "bold"),
        legend.text = element_text(size = 11),
        plot.caption = element_text(margin = margin(t = 8),
                                    hjust = 0)) +
  labs(x = NULL, y = "Percent of votes",
       title = "Percentage of votes by party, Athabasca 1991-2016",
       caption = "Data source: Elections Saskatchewan, Election Results. https://www.elections.sk.ca/election-results/")

## 2.3. plot party percentages, by ethnic composition
plot_percent_ethnic <- party_percent_ethnic %>% 
  ggplot(aes(x = year, y = percent, color = party)) + 
  geom_point(size = 3) +
  geom_line(size = 1, show.legend = FALSE) +
  facet_wrap(~ ethnic, nrow = 2, 
             labeller = labeller(ethnic = c(
               FND = "First Nations over 75%",
               FNM = "First Nations 60% to 75%",
               MTD = "Metis over 75%",
               MTM = "Metis 60% to 75%",
               MX = "Mixed"))) +
  scale_x_continuous(limits = c(1990, 2017), 
                     breaks = c(seq(1991, 2013, by = 4), 2016)) +
  scale_y_continuous(breaks = seq(0, 100, by = 10)) +
  scale_color_manual(name = "Party",
                     values = c(NDP = "darkorange", 
                                Saskatchewan = "darkgreen",
                                Liberal = "red2",
                                "Progress. Cons." = "navy",
                                Green = "yellowgreen",
                                Independents = "grey35")) +
  theme_bw() +
  theme(plot.title = element_text(hjust = .5, size = 13,
                                  margin = margin(b = 8),
                                  face = "bold"),
        panel.grid.major = element_line(colour = "gray"),
        panel.grid.minor = element_blank(),
        axis.title = element_text(size = 12, face = "bold"),
        axis.title.y = element_text(margin = margin(r = 8)),
        axis.text.x = element_text(angle = 45, hjust = 1),
        axis.text = element_text(size = 10),
        strip.text = element_text(size = 11),
        legend.title = element_text(size = 11, face = "bold"),
        legend.text = element_text(size = 11),
        legend.position = c(.95, 0),
        legend.justification = c("right", "bottom"),
        plot.caption = element_text(margin = margin(t = 8),
                                    hjust = 0)) +
  labs(x = NULL, y = "Percent of votes",
       title = "Voting patterns by communities' main ethnicity, Athabasca 1991-2016",
       caption = "Elections data: Elections Saskatchewan, Election Results. https://www.elections.sk.ca/election-results/\nEthnic composition data: Statistics Canada, Census Program. https://www12.statcan.gc.ca/census-recensement/index-eng.cfm")

## 2.4. plot party percentages for select communities:
plot_percent_select <- party_percent_select %>% 
  ggplot(aes(x = year, y = percent, color = party)) + 
  geom_point(size = 3) +
  geom_line(size = 1, show.legend = FALSE) +
  facet_wrap(~ poll, nrow = 2) +
  scale_x_continuous(limits = c(1990, 2017), 
                     breaks = c(seq(1991, 2013, by = 4), 2016)) +
  scale_y_continuous(breaks = seq(0, 100, by = 10)) +
  scale_color_manual(name = "Party",
                     values = c(NDP = "darkorange", 
                                Saskatchewan = "darkgreen",
                                Liberal = "red2",
                                "Progress. Cons." = "navy",
                                Green = "yellowgreen",
                                Independents = "grey35")) +
  theme_bw() +
  theme(plot.title = element_text(hjust = .5, size = 13,
                                  margin = margin(b = 8),
                                  face = "bold"),
        panel.grid.major = element_line(colour = "gray"),
        panel.grid.minor = element_blank(),
        axis.title = element_text(size = 12, face = "bold"),
        axis.title.y = element_text(margin = margin(r = 8)),
        axis.text = element_text(size = 10),
        axis.text.x = element_text(angle = 45, hjust = 1),
        strip.text = element_text(size = 11),
        legend.title = element_text(size = 11, face = "bold"),
        legend.text = element_text(size = 11),
        legend.position = c(.95, 0),
        legend.justification = c("right", "bottom"),
        plot.caption = element_text(margin = margin(t = 8),
                                    hjust = 0)) +
  labs(x = NULL, y = "Percent of votes", 
       title = "Voting patterns in select communities, Athabasca 1991-2016",
       caption = "Elections data: Elections Saskatchewan, Election Results. https://www.elections.sk.ca/election-results/")

## 2.5. plot overall voter turnout
plot_turnout <- turnout %>% 
  ggplot(aes(x = year, y = turnout)) + 
  geom_line(size = 1.2, color = "red3") +
  geom_label(aes(label = turnout), 
             color = "red3",
             fontface = "bold",
             label.size = .4) + # label border thickness
  scale_x_continuous(limits = c(1990, 2017), 
                     breaks = c(seq(1991, 2013, by = 4), 2016)) +
  scale_y_continuous(breaks = seq(0, 90, by = 10)) +
  theme_bw() +
  theme(plot.title = element_text(hjust = .5, size = 13,
                                  margin = margin(b = 8),
                                  face = "bold"),
        panel.grid.major = element_line(colour = "gray"),
        panel.grid.minor = element_blank(),
        axis.title = element_text(size = 12, face = "bold"),
        axis.title.y = element_text(margin = margin(r = 8)),
        axis.text = element_text(size = 10),
        plot.caption = element_text(margin = margin(t = 8),
                                    hjust = 0)) +
  labs(x = NULL, y = "Voter turnout, percent",
       title = "Voter turnout in Athabasca Constituency, 1991-2016",
       caption = "Elections data: Elections Saskatchewan, Election Results. https://www.elections.sk.ca/election-results/")

## 2.6. plot voter turnout by ethnic composition
plot_turnout_ethnic <- turnout_ethnic %>% 
  ggplot(aes(x = year, y = turnout, color = ethnic)) + 
  geom_point(size = 3) +
  geom_line(size = 1.2, show.legend = FALSE) +
  geom_label_repel(aes(label = turnout),
                   fontface = "bold",
                   label.size = .4, # label border thickness
                   size = 3, # label size
                   force = .02, # repelling force
                   show.legend = FALSE) +
  scale_x_continuous(limits = c(1990, 2017), 
                     breaks = c(seq(1991, 2013, by = 4), 2016)) +
  scale_y_continuous(breaks = seq(0, 90, by = 10)) +
  scale_color_manual(name = "Ethnic composition",
                     labels = c("First Nations\nover 75%",
                                "First Nations\n60% to 75%",
                                "Metis\nover 75%",
                                "Metis\n60% to 75%",
                                "Mixed"),
                     values = c(FND = "darkorange4", 
                                FNM = "sienna1",
                                MTD = "deepskyblue4",
                                MTM = "deepskyblue",
                                MX = "gray45")) +
  theme_bw() +
  theme(plot.title = element_text(hjust = .5, size = 13,
                                  margin = margin(b = 8),
                                  face = "bold"),
        panel.grid.major = element_line(colour = "gray"),
        panel.grid.minor = element_blank(),
        axis.title = element_text(size = 12, face = "bold"),
        axis.title.y = element_text(margin = margin(r = 8)),
        axis.text = element_text(size = 10),
        legend.title = element_text(size = 11, face = "bold"),
        legend.text = element_text(size = 11),
        legend.key.height = unit(2, "lines"), # increases intervals between legend items
        plot.caption = element_text(margin = margin(t = 8),
                                    hjust = 0)) +
  labs(x = NULL, y = "Voter turnout, percent",
       title = "Voter turnout by communities' ethnic composition,\nAthabasca 1991-2016",
       caption = "Elections data: Elections Saskatchewan, Election Results. https://www.elections.sk.ca/election-results/\nEthnic composition data: Statistics Canada, Census Program. https://www12.statcan.gc.ca/census-recensement/index-eng.cfm")

## 2.7. plot voter turnout in select communities
plot_turnout_select <- turnout_select %>% 
  ggplot(aes(x = year, y = turnout, color = poll)) + 
  geom_point(size = 3) +
  geom_line(size = 1.2, show.legend = FALSE) +
  geom_label_repel(aes(label = turnout),
                   fontface = "bold",
                   label.size = .4, # label border thickness
                   size = 3, # label size
                   force = .02, # repelling force
                   show.legend = FALSE) +
  scale_x_continuous(limits = c(1990, 2017), 
                     breaks = c(seq(1991, 2013, by = 4), 2016)) +
  scale_y_continuous(breaks = seq(0, 90, by = 10)) +
  scale_color_manual(name = "Community",
                     labels = c("Beauval",
                                "Cole Bay",
                                "Ile-X",
                                "Jans Bay",
                                "La Loche"),
                     values = c("Beauval" = "forestgreen", 
                                "Cole Bay" = "gray40",
                                "Ile a la Crosse" = "deepskyblue4",
                                "Jans Bay" = "darkorange2",
                                "La Loche" = "red3")) +
  theme_bw() +
  theme(plot.title = element_text(hjust = .5, size = 13,
                                  margin = margin(b = 8),
                                  face = "bold"),
        panel.grid.major = element_line(colour = "gray"),
        panel.grid.minor = element_blank(),
        axis.title = element_text(size = 12, face = "bold"),
        axis.title.y = element_text(margin = margin(r = 8)),
        axis.text = element_text(size = 10),
        legend.title = element_text(size = 11, face = "bold"),
        legend.text = element_text(size = 11),
        plot.caption = element_text(margin = margin(t = 8),
                                    hjust = 0)) +
  labs(x = NULL, y = "Voter turnout, percent",
       title = "Voter turnout in select communities, Athabasca 1991-2016",
       caption = "Elections data: Elections Saskatchewan, Election Results. https://www.elections.sk.ca/election-results/")

## 2.8. plot number of eligible voters by communities' ethnic composition
plot_eligible_ethnic <-
  eligible_ethnic %>% 
  ggplot(aes(x = year, y = voters_ethnic, fill = ethnic)) +
  geom_col(position = "dodge", width = 3) +
  scale_x_continuous(breaks = c(seq(1991, 2013, by = 4), 2016)) +
  scale_y_continuous(breaks = seq(0, 3000, by = 250)) +
  scale_fill_manual(name = "Ethnic composition",
                    labels = c("First Nations\nover 75%",
                               "First Nations\n60% to 75%",
                               "Metis\nover 75%",
                               "Metis\n60% to 75%",
                               "Mixed"),
                    values = c(FND = "darkorange4", 
                               FNM = "sienna1",
                               MTD = "deepskyblue4",
                               MTM = "deepskyblue",
                               MX = "gray45")) +
  theme_bw() +
  theme(plot.title = element_text(hjust = .5, size = 13,
                                  margin = margin(b = 8),
                                  face = "bold"),
        panel.grid.major = element_line(colour = "gray"),
        panel.grid.minor = element_blank(),
        axis.title = element_text(size = 12, face = "bold"),
        axis.title.y = element_text(margin = margin(r = 8)),
        axis.text = element_text(size = 10),
        legend.title = element_text(size = 11, face = "bold"),
        legend.text = element_text(size = 11),
        legend.position = "bottom",
        legend.key.width = unit(.4, "cm"),
        plot.caption = element_text(margin = margin(t = 8),
                                    hjust = 0)) +
  labs(x = NULL, y = "Eligible voters",
       title = "Number of Eligible Voters by Communities' Ethnic Composition\nAthabasca 1991-2016",
       caption = "Data source: Elections Saskatchewan, Election Results. https://www.elections.sk.ca/election-results/\nEthnic composition data: Statistics Canada, Census Program. https://www12.statcan.gc.ca/census-recensement/index-eng.cfm")

### Step 3 export data and plots to external files

## 3.1. export numeric data
write_csv(elections_data, path = "elections_by_year_all_polls.csv")
write_csv(elections_data_long, path = "elections_all_polls_long_format.csv")
write_csv(eligible_ethnic, path = "eligible_voters_by_main_ethnicity.csv")

## 3.2. export plots as .svg
graph2svg(plot_votes, file = "votes_by_party_number")
graph2svg(plot_percent, file = "votes_by_party_percent")
graph2svg(plot_percent_ethnic, file = "votes_by_ethnicity")
graph2svg(plot_percent_select, file = "votes_by_community")
graph2svg(plot_eligible_ethnic, file = "voters_by_communs_ethnic_comp")
graph2svg(plot_turnout, file = "turnout")
graph2svg(plot_turnout_ethnic, file = "turnout_by_ethnicity")
graph2svg(plot_turnout_select, file = "turnout_by_community")

## 3.3. export plots as .pptx
graph2ppt(plot_votes, file = "votes_by_party_number")
graph2ppt(plot_percent, file = "votes_by_party_percent")
graph2ppt(plot_percent_ethnic, file = "votes_by_ethnicity")
graph2ppt(plot_percent_select, file = "votes_by_community")
graph2ppt(plot_eligible_ethnic, file = "voters_by_communs_ethnic_comp")
graph2ppt(plot_turnout, file = "turnout")
graph2ppt(plot_turnout_ethnic, file = "turnout_by_ethnicity")
graph2ppt(plot_turnout_select, file = "turnout_by_community")
